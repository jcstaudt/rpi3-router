#!/bin/bash

sudo apt-get update                                        # Update all repositories
sudo apt-get upgrade                                       # Apply all existing updates
sudo sed -i -e 's/gpu_mem=64/gpu_mem=16/' /boot/config.txt # Reduce RAM available to GPU from 64->16MB
sudo apt-get install --force -y hostapd isc-dhcp-server
sudo cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.original
# TODO: Currently assuming the pwd is inline with this script! Bad!
sudo cp ../config/dhcpd.conf /etc/dhcp/dhcpd.conf
sudo cp ../config/isc-dhcp-server /etc/default/isc-dhcp-server
sudo ifdown wlan0
sudo cp /etc/network/interfaces /etc/network/interfaces.original
sudo cp ../config/interfaces /etc/network/interfaces.rpi3-router
sudo ln -fs /etc/network/interfaces.rpi3-router /etc/network/interfaces
sudo ifconfig wlan0 192.168.42.1 # TODO: Make interface and IP address
                                 #       configurable
sudo cp ../config/hostapd.conf /etc/hostapd/hostapd.conf
sudo cp /etc/sysctl.conf /etc/sysctl.conf.original
sudo cp ../config/sysctl.conf /etc/sysctl.conf
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"
sudo update-rc.d hostapd enable
sudo update-rc.d isc-dhcp-server enable
sudo shutdown -r now
